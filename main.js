import init, { run_app } from './pkg/testweb.js';
async function main() {
   await init('/pkg/testweb_bg.wasm');
   run_app();
}
main()
