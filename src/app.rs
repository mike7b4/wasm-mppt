use serde::Deserialize;
use std::convert::TryFrom;
use std::io::{Error, ErrorKind};
use yew::format::Text;
use yew::prelude::*;
use yew::services::websocket::{WebSocketService, WebSocketStatus, WebSocketTask};
#[derive(Deserialize, Clone, Default)]
pub struct RealTimeStatus {
    utc: String,
    solary_voltage: f32,
    solary_current: f32,
    solary_power: f32,
    battery_voltage: f32,
    battery_current: f32,
    battery_power: f32,
    load_voltage: f32,
    load_current: f32,
    load_power: f32,
    #[serde(skip)]
    pub modified: bool,
}

fn next(s: Option<&str>) -> Result<f32, Error> {
    let s: f32 = s
        .ok_or(Error::new(ErrorKind::Other, "Real time status"))?
        .parse()
        .map_err(|_| Error::new(ErrorKind::Other, "Real time status"))?;
    Ok(s)
}

impl TryFrom<&String> for RealTimeStatus {
    type Error = Box<dyn std::error::Error>;
    fn try_from(status: &String) -> Result<Self, Box<dyn std::error::Error>> {
        let e = Box::new(Error::new(ErrorKind::Other, "Real time status"));
        let mut sep = status.lines().next().ok_or(e)?.split(',');
        Ok(Self {
            utc: sep.next().unwrap_or(&"?").to_string(),
            solary_voltage: next(sep.next())?,
            solary_current: next(sep.next())?,
            solary_power: next(sep.next())?,
            battery_voltage: next(sep.next())?,
            battery_current: next(sep.next())?,
            battery_power: next(sep.next())?,
            load_voltage: next(sep.next())?,
            load_current: next(sep.next())?,
            load_power: next(sep.next())?,
            modified: true,
        })
    }
}

pub struct App {
    link: ComponentLink<Self>,
    ws: WebSocketTask,
    ws_status: WebSocketStatus,
    data: Vec<RealTimeStatus>,
}

#[derive(Debug)]
pub enum Msg {
    WsData(Text),
    StatusChange(WebSocketStatus),
}

/// This type is an expected response from a websocket connection.
#[derive(Deserialize, Debug)]
pub struct WsResponse {
    value: u32,
}

impl Component for App {
    type Message = Msg;
    type Properties = ();

    fn create(_: Self::Properties, link: ComponentLink<Self>) -> Self {
        let callback = link.callback(Msg::WsData);
        let notification = link.callback(Msg::StatusChange);
        App {
            ws: WebSocketService::connect_text("ws://localhost:4040", callback, notification)
                .unwrap(),
            link,
            data: Vec::new(),
            ws_status: WebSocketStatus::Closed,
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::WsData(data) => {
                let data = data.unwrap();
                if let Ok(data) = RealTimeStatus::try_from(&data) {
                    self.data.push(data);
                }
            }
            Msg::StatusChange(status) => self.ws_status = status,
        }
        true
    }

    fn view(&self) -> Html {
        let def = RealTimeStatus::default();
        let status = self.data.last().unwrap_or(&def);
        let mut m = Vec::new();
        m.push(("Last update", status.utc.clone()));
        m.push(("Solary voltage", format!("{} Volt", status.solary_voltage)));
        m.push(("Solary current", format!("{} Amp", status.solary_current)));
        m.push(("Solary power", format!("{} Watt", status.solary_power)));
        m.push((
            "Battery voltage",
            format!("{} Volt", status.battery_voltage),
        ));
        m.push(("Battery current", format!("{} Amp", status.battery_current)));
        m.push(("Battery power", format!("{} Watt", status.battery_power)));
        m.push(("Load voltage", format!("{} Volt", status.load_voltage)));
        m.push(("Load current", format!("{} Amp", status.load_current)));
        m.push(("Load power", format!("{} Watt", status.load_power)));
        html! {
            <div class="article">
            <h1>{ format!("CAN bus link is {:?}", &self.ws_status) }</h1>
            <table class="table">
            <tr><th>{"What"}</th><th>{"Value"}</th></tr>
            { m.iter().map(|(k,v)|{html!{<tr><td>{k}</td><td>{v}</td></tr>}}).collect::<Html>() }</table>
            </div>
        }
    }

    fn change(&mut self, _props: Self::Properties) -> ShouldRender {
        false
    }
}
